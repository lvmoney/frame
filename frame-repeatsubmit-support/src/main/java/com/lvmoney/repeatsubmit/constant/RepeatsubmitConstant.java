package com.lvmoney.repeatsubmit.constant;/**
 * 描述:
 * 包名:com.lvmoney.k8s.base.vo.jyaml
 * 版本信息: 版本1.0
 * 日期:2019/8/19
 * Copyright XXXXXX科技有限公司
 */


/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/8/19 8:58
 */
public class RepeatsubmitConstant {

    /**
     * 是否引入重复提交的配置文件值
     */
    public static final String FRAME_REPEAT_SUPPORT_FALSE = "false";
    /**
     * 是否引入重复提交的配置文件值
     */
    public static final String FRAME_REPEAT_SUPPORT_TRUE = "true";

    /**
     * 连接符下划线
     */
    public static final String CONNECTOR_UNDERLINE = "_";

}
