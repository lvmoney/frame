package com.lvmoney.bigdata.common.regression;/**
 * 描述:
 * 包名:com.lvmoney.bigdata.common.regression
 * 版本信息: 版本1.0
 * 日期:2019/12/4
 * Copyright XXXXXX科技有限公司
 */


import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/12/4 10:17
 */
public class LinearTest {
    public static void main(String[] args) throws IOException {
        //read Sample.txt
        Sample[] sam = new Sample[25];
        int w = 0;

        long filePoint = 0;
        String s;
        RandomAccessFile file = new RandomAccessFile("D:\\data\\temp\\LinearSample.txt", "r");
        long fileLength = file.length();

        while (filePoint < fileLength) {
            s = file.readLine();
            //s --> sample
            String[] sub = s.split(" ");
            sam[w] = new Sample(sub.length - 1);
            for (int i = 0; i < sub.length; i++) {
                if (i == sub.length - 1) {
                    sam[w].value = Double.parseDouble(sub[i]);
                } else {
                    sam[w].features[i] = Double.parseDouble(sub[i]);
                }
            }//for
            w++;
            filePoint = file.getFilePointer();
        }//while read file

        LinearRegression lr = new LinearRegression();
        double[] para = {0, 0, 0, 0, 0};
        double rate = 0.5;
        double th = 0.001;
        lr.Initialize(sam, w);
        lr.setPara(para, rate, th);
        lr.Update();
        lr.OutputTheta();
    }
}
