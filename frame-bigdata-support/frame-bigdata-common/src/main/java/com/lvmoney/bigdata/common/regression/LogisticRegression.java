package com.lvmoney.bigdata.common.regression;/**
 * 描述:
 * 包名:com.lvmoney.bigdata.common.regression
 * 版本信息: 版本1.0
 * 日期:2019/12/4
 * Copyright XXXXXX科技有限公司
 */


/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/12/4 10:12
 */
public class LogisticRegression extends Regression {
    @Override
    public double PreVal(Sample s) {
        double val = 0;
        for (int i = 0; i < paraNum; i++) {
            val += theta[i] * s.features[i];
        }
        return 1 / (1 + Math.pow(Math.E, -val));
    }

    @Override
    public double CostFun() {
        double sum = 0;
        for (int i = 0; i < samNum; i++) {
            double p = PreVal(sam[i]);
            double d = Math.log(p) * sam[i].label + (1 - sam[i].label) * Math.log(1 - p);
            sum += d;
        }
        return -1 * (sum / samNum);
    }

    @Override
    public void Update() {
        double former = 0; // the cost before update
        double latter = CostFun(); // the cost after update
        double d = 0;
        double[] p = new double[paraNum];
        do {
            former = latter;
            //update theta
            for (int i = 0; i < paraNum; i++) {
                // for theta[i]
                for (int j = 0; j < samNum; j++) {
                    d += (PreVal(sam[j]) - sam[j].value) * sam[j].features[i];
                }
                p[i] -= (rate * d) / samNum;
            }
            latter = CostFun();

            if (former - latter < 0) {
                System.out.println("α is larger!!!");
                break;
            }
        } while (former - latter > th);
        theta = p;
    }
}
