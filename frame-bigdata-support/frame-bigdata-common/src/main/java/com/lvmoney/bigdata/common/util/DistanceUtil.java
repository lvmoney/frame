package com.lvmoney.bigdata.common.util;/**
 * 描述:
 * 包名:com.lvmoney.cache.common.annation
 * 版本信息: 版本1.0
 * 日期:2019/11/20
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.bigdata.common.vo.Point;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/20 11:06
 */
public class DistanceUtil {
    /**
     * 求两个点的欧式距离
     *
     * @param p1: 点
     * @param p2: 点
     * @return: void
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/9/9 19:39
     */
    public static double getEuclideanDis(Point p1, Point p2) {
        double count_dis = 0;
        float[] p1LocalArray = p1.getLocalArray();
        float[] p2LocalArray = p2.getLocalArray();

        if (p1LocalArray.length != p2LocalArray.length) {
            throw new IllegalArgumentException("length of array must be equal!");
        }

        for (int i = 0; i < p1LocalArray.length; i++) {
            count_dis += Math.pow(p1LocalArray[i] - p2LocalArray[i], 2);
        }

        return Math.sqrt(count_dis);
    }
}

