package com.lvmoney.bigdata.common.vo;/**
 * 描述:
 * 包名:com.lvmoney.cache.common.annation
 * 版本信息: 版本1.0
 * 日期:2019/11/20
 * Copyright XXXXXX科技有限公司
 */

import lombok.Data;

import java.io.Serializable;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/20 11:06
 */
@Data
public class Point implements Serializable {
    private float[] localArray;
    private int id;
    /**
     * 标识属于哪个类中心
     */
    private int clusterId;
    /**
     * 标识和所属类中心的距离。
     */
    private float dist;

    public Point(int id, float[] localArray) {
        this.id = id;
        this.localArray = localArray;
    }

    public Point(float[] localArray) {
        /**
         * 表示不属于任意一个类
         */
        this.id = -1;
        this.localArray = localArray;
    }

    @Override
    public String toString() {
        String result = "Point_id=" + id + "  [";
        for (int i = 0; i < localArray.length; i++) {
            result += localArray[i] + " ";
        }
        return result.trim() + "] clusterId: " + clusterId + " dist: " + dist;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Point point = (Point) obj;
        if (point.localArray.length != localArray.length) {
            return false;
        }

        for (int i = 0; i < localArray.length; i++) {
            if (Float.compare(point.localArray[i], localArray[i]) != 0) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        float x = localArray[0];
        float y = localArray[localArray.length - 1];
        long temp = x != +0.0d ? Double.doubleToLongBits(x) : 0L;
        int result = (int) (temp ^ (temp >>> 32));
        temp = y != +0.0d ? Double.doubleToLongBits(y) : 0L;
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}