package com.lvmoney.bigdata.common.vo;/**
 * 描述:
 * 包名:com.lvmoney.cache.common.annation
 * 版本信息: 版本1.0
 * 日期:2019/11/20
 * Copyright XXXXXX科技有限公司
 */

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/20 11:06
 */
@Data
public class Cluster implements Serializable {
    private static final long serialVersionUID = 4085657343491016045L;
    /**
     * id
     */
    private int id;
    /**
     * 中心
     */
    private Point center;
    /**
     * 成员
     */
    private List<Point> members = new ArrayList<Point>();

    public Cluster(int id, Point center) {
        this.id = id;
        this.center = center;
    }

    public Cluster(int id, Point center, List<Point> members) {
        this.id = id;
        this.center = center;
        this.members = members;
    }

    public void addPoint(Point newPoint) {
        if (!members.contains(newPoint)) {
            members.add(newPoint);
        } else {
            System.out.println("样本数据点 {" + newPoint.toString() + "} 已经存在！");
        }
    }

    @Override
    public String toString() {
        String toString = "Cluster \n" + "Cluster_id=" + this.id + ", center:{" + this.center.toString() + "}";
        for (Point point : members) {
            toString += "\n" + point.toString();
        }
        return toString + "\n";
    }


}
