package com.lvmoney.bigdata.common.util;/**
 * 描述:
 * 包名:com.lvmoney.bigdata.common.util
 * 版本信息: 版本1.0
 * 日期:2019/12/3
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.common.constant.CommonConstant;
import com.lvmoney.common.util.JsonUtil;
import org.ansj.domain.Result;
import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.DicAnalysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @describe：是一种用于信息检索与数据挖掘的常用加权技术
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/12/3 9:43
 */
public class TFIDFUtil {

    /**
     * 文档表
     */
    private List<String> documents;
    /**
     * 文档与词汇列表
     */
    private List<List<String>> documentWords;
    /**
     * 文档词频统计表
     */
    private List<Map<String, Integer>> docuementTfList;
    /**
     * 词汇出现文档数统计表
     */
    private Map<String, Double> idfMap;

    /**
     * 是否只抽取名词
     */
    private boolean onlyNoun;

    public TFIDFUtil(List<String> documents, boolean onlyNoun) {
        this.documents = documents;
        this.onlyNoun = onlyNoun;
    }

    public List<Map<String, Double>> exec() {
        this.splitDoc();
        this.getTf();
        this.getIdf();
        return getTfidf();
    }

    /**
     * 获取所有文档数，用于逆文档频率 IDF 的计算
     *
     * @throws
     * @return: int
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/12/3 10:04
     */
    private int getDocumentCount() {
        return documents.size();
    }

    /**
     * 对每一个文档进行词语切分
     *
     * @throws
     * @return: void
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/12/3 10:04
     */
    private void splitDoc() {
        documentWords = new ArrayList();
        for (String document : documents) {
            Result splitWordRes = DicAnalysis.parse(document);
            List<String> wordList = new ArrayList();
            for (Term term : splitWordRes.getTerms()) {
                if (onlyNoun) {
                    if (term.getNatureStr().equals("n") || term.getNatureStr().equals("ns") || term.getNatureStr().equals("nz")) {
                        wordList.add(term.getName());
                    }
                } else {
                    wordList.add(term.getName());
                }
            }
            documentWords.add(wordList);
        }
    }


    /**
     * 对每一个文档进行词频的计算
     *
     * @throws
     * @return: void
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/12/3 10:04
     */
    private void getTf() {
        docuementTfList = new ArrayList();
        for (List<String> wordList : documentWords) {
            Map<String, Integer> countMap = new HashMap(CommonConstant.MAP_DEFAULT_SIZE);
            for (String word : wordList) {
                if (countMap.containsKey(word)) {
                    countMap.put(word, countMap.get(word) + 1);
                } else {
                    countMap.put(word, 1);
                }
            }
            docuementTfList.add(countMap);
        }
    }


    /**
     * 计算逆文档频率 IDF
     *
     * @throws
     * @return: void
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/12/3 10:05
     */
    private void getIdf() {
        int documentCount = getDocumentCount();
        idfMap = new HashMap();
        // 统计词语在多少文档里面出现了
        Map<String, Integer> wordAppearendMap = new HashMap(CommonConstant.MAP_DEFAULT_SIZE);
        for (Map<String, Integer> countMap : docuementTfList) {
            for (String word : countMap.keySet()) {
                if (wordAppearendMap.containsKey(word)) {
                    wordAppearendMap.put(word, wordAppearendMap.get(word) + 1);
                } else {
                    wordAppearendMap.put(word, 1);
                }
            }
        }
        // 对每个词语进行计算
        for (String word : wordAppearendMap.keySet()) {
            double idf = Math.log(documentCount / (wordAppearendMap.get(word) + 1));
            idfMap.put(word, idf);
        }
    }

    /**
     * 得到文档的tf-idf
     *
     * @throws
     * @return: java.util.List<java.util.Map < java.lang.String, java.lang.Double>>
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/12/3 11:27
     */
    private List<Map<String, Double>> getTfidf() {
        List<Map<String, Double>> tfidfRes = new ArrayList<Map<String, Double>>();
        for (Map<String, Integer> docuementTfMap : docuementTfList) {
            Map<String, Double> tfIdf = new HashMap(CommonConstant.MAP_DEFAULT_SIZE);
            for (String word : docuementTfMap.keySet()) {
                double tfidf = idfMap.get(word) * docuementTfMap.get(word);
                tfIdf.put(word, tfidf);
            }
            tfidfRes.add(tfIdf);
        }
        return tfidfRes;
    }

    public static void main(String[] args) throws IOException {
        List<String> list = new ArrayList() {{
            add(readFile2String("D:\\data\\temp\\book1.txt"));
            add(readFile2String("D:\\data\\temp\\book2.txt"));
            add(readFile2String("D:\\data\\temp\\book3.txt"));
            add(readFile2String("D:\\data\\temp\\book4.txt"));
        }};
        TFIDFUtil tfidfUtil = new TFIDFUtil(list, true);
        JsonUtil.t2JsonString(tfidfUtil.exec());
    }

    static final String encode = "UTF-8";
    static final CharsetDecoder cd = Charset.forName(encode).newDecoder();

    public static String readFile2String(String path) throws IOException {
        FileChannel channel = new FileInputStream(new File(path)).getChannel();
        ByteBuffer buf = ByteBuffer.allocate(1024);
        CharBuffer cBuf = CharBuffer.allocate(1024);
        int bytesRead = channel.read(buf);
        StringBuilder sb = new StringBuilder();
        int length = 0;
        while (bytesRead != -1) {
            buf.flip();
            //指定未完待续
            cd.decode(buf, cBuf, false);
            cBuf.flip();
            length += cBuf.limit();
            sb.append(new String(cBuf.array(), 0, cBuf.limit()));
            cBuf.clear();
            buf.compact();
            bytesRead = channel.read(buf);
        }
        return sb.toString();
    }

}
