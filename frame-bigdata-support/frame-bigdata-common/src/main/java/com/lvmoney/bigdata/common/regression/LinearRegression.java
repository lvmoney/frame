package com.lvmoney.bigdata.common.regression;/**
 * 描述:
 * 包名:com.lvmoney.bigdata.common.regression
 * 版本信息: 版本1.0
 * 日期:2019/12/4
 * Copyright XXXXXX科技有限公司
 */


/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/12/4 10:11
 */
public class LinearRegression extends Regression {
    @Override
    public double PreVal(Sample s) {
        double val = 0;
        for (int i = 0; i < paraNum; i++) {
            val += theta[i] * s.features[i];
        }
        return val;
    }

    @Override
    public double CostFun() {
        double sum = 0;
        for (int i = 0; i < samNum; i++) {
            double d = PreVal(sam[i]) - sam[i].value;
            sum += Math.pow(d, 2);
        }
        return sum / (2 * samNum);
    }

    @Override
    public void Update() {
        double former = 0; // the cost before update
        double latter = CostFun(); // the cost after updatedouble[] p = new double[paraNum];
        double[] p = new double[paraNum];
        do {
            former = latter;
            //update theta
            for (int i = 0; i < paraNum; i++) {
                // for theta[i]
                double d = 0;
                for (int j = 0; j < samNum; j++) {
                    d += (PreVal(sam[j]) - sam[j].value) * sam[j].features[i];
                }
                p[i] -= (rate * d) / samNum;
            }
            theta = p;
            latter = CostFun();

            if (former - latter < 0) {
                System.out.println("α is larger!!!");
                break;
            }
        } while (former - latter > th);
    }

}
