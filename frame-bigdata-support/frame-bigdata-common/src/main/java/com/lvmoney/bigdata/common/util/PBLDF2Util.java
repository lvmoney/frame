package com.lvmoney.bigdata.common.util;/**
 * 描述:
 * 包名:com.lvmoney.cache.common.annation
 * 版本信息: 版本1.0
 * 日期:2019/11/20
 * Copyright XXXXXX科技有限公司
 */

import com.lvmoney.common.util.Md5Util;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

/**
 * @describe：加盐工具,类似md5，比md5更安全
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/20 11:06
 */
public class PBLDF2Util {
    public static final String PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA1";


    public static final int SALT_BYTE_SIZE = 32 / 2;         //盐的长度
    public static final int HASH_BIT_SIZE = 128 * 4;         //生成密文的长度
    public static final int PBKDF2_ITERATIONS = 1000;        //迭代次数


    /**
     * 对输入的密码，加盐后的密码，以及盐值进行校验
     *
     * @param attemptedPassword: 原密码
     * @param encryptedPassword: 加密加盐后的密文
     * @param salt:              盐值
     * @throws
     * @return: boolean
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/12/3 9:31
     */
    public static boolean authenticate(String attemptedPassword, String encryptedPassword, String salt)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        // 用相同的盐值对用户输入的密码进行加密
        String encryptedAttemptedPassword = getEncryptedPassword(attemptedPassword, salt);
        // 把加密后的密文和原密文进行比较，相同则验证成功，否则失败
        return encryptedAttemptedPassword.equals(encryptedPassword);
    }


    /**
     * 给原始密码加盐
     *
     * @param password:
     * @param salt:
     * @throws
     * @return: java.lang.String
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/12/3 9:32
     */
    public static String getEncryptedPassword(String password, String salt) throws NoSuchAlgorithmException,
            InvalidKeySpecException {

        KeySpec spec = new PBEKeySpec(password.toCharArray(), fromHex(salt), PBKDF2_ITERATIONS, HASH_BIT_SIZE);
        SecretKeyFactory f = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
        return toHex(f.generateSecret(spec).getEncoded());
    }


    /**
     * 通过加密的强随机数生成盐(最后转换为16进制)
     *
     * @throws
     * @return: java.lang.String
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/12/3 9:32
     */
    public static String generateSalt() throws NoSuchAlgorithmException {
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[SALT_BYTE_SIZE];
        random.nextBytes(salt);

        return toHex(salt);
    }


    /**
     * 十六进制字符串转二进制字符串
     *
     * @param hex:
     * @throws
     * @return: byte[]
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/12/3 9:32
     */
    private static byte[] fromHex(String hex) {
        byte[] binary = new byte[hex.length() / 2];
        for (int i = 0; i < binary.length; i++) {
            binary[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return binary;
    }


    /**
     * 二进制字符串转十六进制字符串
     *
     * @param array:
     * @throws
     * @return: java.lang.String
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/12/3 9:33
     */
    private static String toHex(byte[] array) {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            return String.format("%0" + paddingLength + "d", 0) + hex;
        } else {
            return hex;
        }
    }


    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String password = "123456";

        String salt = PBLDF2Util.generateSalt();
        String pbkdf2 = PBLDF2Util.getEncryptedPassword(password, salt);
        String md5 = Md5Util.digest(password);

        System.out.println("原始密码:" + password);
        System.out.println("MD5加密后的密码:" + md5);
        System.out.println("盐值:" + salt);
        System.out.println("PBKDF2加盐后的密码:" + pbkdf2);
        System.out.println("Test success");

        System.out.println(PBLDF2Util.authenticate(password, pbkdf2, salt));
    }
}
