package com.lvmoney.avro.test;/**
 * 描述:
 * 包名:com.lvmoney.test
 * 版本信息: 版本1.0
 * 日期:2019/11/4
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.avro.vo.User;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.io.DatumReader;
import org.apache.avro.specific.SpecificDatumReader;

import java.io.File;
import java.io.IOException;

/**
 * @describe：解析通过AvroSerializerTest方法生成的avro文件
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/4 17:10
 */
public class AvroDeSerializerTest {
    public static void main(String[] args) throws IOException {

        DatumReader<User> userDatumReader = new SpecificDatumReader<User>(User.class);
        DataFileReader<User> dataFileReader = new DataFileReader<User>(new File("users.avro"), userDatumReader);
        User user = null;
        while (dataFileReader.hasNext()) {
            user = dataFileReader.next(user);
            System.out.println(user);
        }
    }
}
