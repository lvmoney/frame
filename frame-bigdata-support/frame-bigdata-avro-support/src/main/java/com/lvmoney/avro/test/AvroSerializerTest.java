package com.lvmoney.avro.test;/**
 * 描述:
 * 包名:com.lvmoney.test
 * 版本信息: 版本1.0
 * 日期:2019/11/4
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.avro.vo.User;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.File;

/**
 * @describe：生成.avro文件，用到实体对象，该实体对象通过resources/user.avsc生成
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/4 17:00
 */
public class AvroSerializerTest {

    public static void main(String[] args) throws Exception {
        User user1 = new User();
        user1.setName("Tom");
        user1.setFavoriteNumber(7);

        User user2 = new User("Jack", 15, "red");

        User user3 = User.newBuilder()
                .setName("Harry")
                .setFavoriteNumber(1)
                .setFavoriteColor("green")
                .build();

        DatumWriter<User> userDatumWriter = new SpecificDatumWriter<>(User.class);
        DataFileWriter<User> dataFileWriter = new DataFileWriter<User>(userDatumWriter);
        dataFileWriter.create(user1.getSchema(), new File("users.avro"));
        dataFileWriter.append(user1);
        dataFileWriter.append(user2);
        dataFileWriter.append(user3);
        dataFileWriter.close();
    }
}
