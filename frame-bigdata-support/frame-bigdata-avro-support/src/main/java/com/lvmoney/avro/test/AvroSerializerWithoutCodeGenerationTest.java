package com.lvmoney.avro.test;/**
 * 描述:
 * 包名:com.lvmoney.test
 * 版本信息: 版本1.0
 * 日期:2019/11/4
 * Copyright XXXXXX科技有限公司
 */


import org.apache.avro.Schema;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumWriter;

import java.io.File;
import java.io.IOException;

/**
 * @describe：通过不生成user对象，序列化到.avro文件
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/4 17:17
 */
public class AvroSerializerWithoutCodeGenerationTest {
    public static void main(String[] args) throws IOException {

        String avscFilePath = AvroSerializerWithoutCodeGenerationTest.class.getClassLoader().getResource("avro/user.avsc").getPath();
        Schema schema = new Schema.Parser().parse(new File(avscFilePath));

        GenericRecord user1 = new GenericData.Record(schema);
        user1.put("name", "Tony");
        user1.put("favorite_number", 18);

        GenericRecord user2 = new GenericData.Record(schema);
        user2.put("name", "Ben");
        user2.put("favorite_number", 3);
        user2.put("favorite_color", "red");

        File file = new File("user2.avro");
        DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<GenericRecord>(schema);
        DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<GenericRecord>(datumWriter);
        dataFileWriter.create(schema, file);
        dataFileWriter.append(user1);
        dataFileWriter.append(user2);
        dataFileWriter.close();
    }
}
