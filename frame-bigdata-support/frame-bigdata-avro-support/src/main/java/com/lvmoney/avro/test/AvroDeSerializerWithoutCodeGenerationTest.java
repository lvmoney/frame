package com.lvmoney.avro.test;/**
 * 描述:
 * 包名:com.lvmoney.test
 * 版本信息: 版本1.0
 * 日期:2019/11/4
 * Copyright XXXXXX科技有限公司
 */


import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;

import java.io.File;
import java.io.IOException;

/**
 * @describe：读取AvroSerializerWithoutCodeGenerationTest生成的.avro文件
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/4 17:19
 */
public class AvroDeSerializerWithoutCodeGenerationTest {
    public static void main(String[] args) throws IOException {
        String avscFilePath =
                AvroDeSerializerWithoutCodeGenerationTest.class.getClassLoader().getResource("avro/user.avsc").getPath();
        Schema schema = new Schema.Parser().parse(new File(avscFilePath));
        File file = new File("user2.avro");
        DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
        DataFileReader<GenericRecord> dataFileReader = new DataFileReader<GenericRecord>(file, datumReader);
        GenericRecord user = null;
        while (dataFileReader.hasNext()) {
            user = dataFileReader.next(user);
            System.out.println(user);
        }
    }
}
