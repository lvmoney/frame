1、使用xxl-job完成分布式调度的客户端，没有去整合官方的server到frame，因为别人的劳动成果
，同时官方的xxl-job还会升级，我们使用时只需要更新jar包版本。把server当做一个分离部署的服务即可    
2、需要结合xxl-job server来完成  
3、可以去官方下载2.1版本（项目resources中有）  
4、xxl-job的使用参考3中的压缩包java代码（导入到idea中直接运行）    
5、xxl-job官方给了各种demo，springboot用的是1.5，本module用的是2.1  
6、client集成yml的配置，启动3中的admin通过http://127.0.0.1:8080/xxl-job-admin进行配置，server已经有配置的demo供参考  
  xxl:  
    job:  
      admin:  
        addresses: http://127.0.0.1:8080/xxl-job-admin#对应server的地址  
      executor:  
        ip:  
        port: 9999  
        logpath: /data/applogs/xxl-job/jobhandler  
        appname: plat-job #注意这个名字要和server中对应  
      accessToken:  