1、cockroach  
http://doc.cockroachchina.baidu.com/#quick-start/start-a-local-cluster/in-docker/  
这个东东支持的是postgresql，同时是国外的，国内用的大厂不多  

2、tidb  
https://pingcap.com/docs-cn/v3.0/reference/best-practices/java-app/  
这个东东主要的问题就是对硬件要求太高了，普通开发者根本玩不了。支持mysql  

3、oceanbase  
https://help.aliyun.com/document_detail/26479.html?spm=5176.doc26478.6.558.xfABL7  
这个我一直觉得应该靠谱，毕竟支付宝双十一都能抗住，但是不开源要收费啊。支持mysql  

个人觉得最好的模式是：支持mysql，支持flink，支持k8s，开源不收费。同时对硬件要求不要太高貌似就perfect了