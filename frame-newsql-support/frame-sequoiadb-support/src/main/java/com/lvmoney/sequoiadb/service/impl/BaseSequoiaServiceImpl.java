package com.lvmoney.sequoiadb.service.impl;/**
 * 描述:
 * 包名:com.lvmoney.sequoiadb.service.impl
 * 版本信息: 版本1.0
 * 日期:2019/11/5
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.sequoiadb.service.BaseSequoiaService;
import com.sequoiadb.base.*;
import com.sequoiadb.exception.BaseException;
import org.bson.BSONObject;
import org.bson.BasicBSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/5 17:30
 */
public class BaseSequoiaServiceImpl implements BaseSequoiaService {

    public Sequoiadb getConnection() {
        ConfigOptions configOptions = new ConfigOptions();
        List<String> connStrings = new ArrayList<String>();
        connStrings.add("192.168.248.128:11810");
        connStrings.add("192.168.248.129:11810");
        connStrings.add("192.168.248.130:11810");
        /**
         * @param connStrings 远程服务器地址"主机:端口"(集合类型)
         * @param username 用户名
         * @param password 密码
         * @param configOptions 数据库连接配置选项
         */
        Sequoiadb sequoiadb = new Sequoiadb(connStrings, "sdbadmin", "sdbadmin", configOptions);
        return sequoiadb;
    }

    public void crtCS() {
        Sequoiadb sequoiadb = getConnection();
        BasicBSONObject options = new BasicBSONObject();
        options.put("PageSize", Sequoiadb.SDB_PAGESIZE_64K);
        options.put("Domain", "testdomain");
        options.put("LobPageSize", 4096);
        /**
         * @param csName 集合空间名称
         * @param options 配置参数，选项如下：
         *		PageSize: 数据页大小。单位为字节，默认值64K。
         *		Domain: 所属域。
         *		LobPageSize: Lob数据页大小。单位为字节，默认值262144。
         * @return 集合空间实例
         */
        sequoiadb.createCollectionSpace("foo", options);
    }

    public void crtCL() {
        Sequoiadb sequoiadb = getConnection();
        BasicBSONObject options = new BasicBSONObject();
        /**
         * 创建普通集合
         *  参数：
         *  ShardingKey ： 分区键
         *  ShardingType ： 分区方式。默认为hash分区。 hash：hash分区，range：范围分区
         *  Compressed ： 标识新集合是否开启数据压缩功能。默认为false
         *  CompressionType : 压缩算法类型。默认为snappy算法。其可选取值如下： snappy，lzw
         *  AutoSplit ： 标识新集合是否开启自动切分功能，默认为false
         * */
        options.put("ShardingKey", new BasicBSONObject("id", 1));
        options.put("ShardingType", "hash");
        options.put("Compressed", true);
        options.put("CompressionType", "lzw");
        options.put("AutoSplit", true);
        //在已经创建的集合空间foo下创建集合bar
        sequoiadb.getCollectionSpace("foo").createCollection("bar", options);
    }

    public void crtMainCL() {
        //获得连接
        Sequoiadb sequoiadb = getConnection();
        /**
         * 创建主集合，主表必须用range切分
         */
        BasicBSONObject mainOptions = new BasicBSONObject();
        // 以字段date作为主集合分区键，此字段为日期类型
        mainOptions.put("ShardingKey", new BasicBSONObject("date", 1));
        mainOptions.put("ShardingType", "range");
        mainOptions.put("IsMainCL", true);
        DBCollection mainCL = sequoiadb.getCollectionSpace("foo").createCollection("maincl", mainOptions);

        /**
         * 创建子集合1（子表既可用range，也可用hash，ShardingKey也不必一定要和主表的一致）
         */
        BasicBSONObject subOptions = new BasicBSONObject();
        subOptions.put("ShardingKey", new BasicBSONObject("id", 1));
        subOptions.put("ShardingType", "hash");
        DBCollection subCL1 = sequoiadb.getCollectionSpace("foo").createCollection("subCL_2017", subOptions);

        /**
         * 创建子集合2
         */
        DBCollection subCL2 = sequoiadb.getCollectionSpace("foo").createCollection("subCL_2018", subOptions);

        /**
         * 将子表1、子表2关联到主表中（将子表附到主表中去，每个子表都有一个范围）
         */
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            BasicBSONObject attachOptions = new BasicBSONObject();
            attachOptions.put("LowBound", new BasicBSONObject("date", format.parse("2017-01-01")));
            attachOptions.put("UpBound", new BasicBSONObject("date", format.parse("2018-01-01")));
            // [2017-01-01, 2018-01-01)
            /**
             * 关联子集合
             * @param csName 子集合名称
             * @param options 分区范围，包含两个字段“LowBound”（区间左值）以及“UpBound”（区间右值），例如：{LowBound:{a:0},UpBound:{a:100}}表示取字段“a”的范围区间：[0, 100)。
             */
            mainCL.attachCollection(subCL1.getFullName(), attachOptions);
            attachOptions.put("LowBound", new BasicBSONObject("date", format.parse("2018-01-01")));
            attachOptions.put("UpBound", new BasicBSONObject("date", format.parse("2019-01-01")));
            // [2018-01-01, 2019-01-01)
            mainCL.attachCollection(subCL2.getFullName(), attachOptions);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         * 可以通过快照查看主子集合情况
         */
        DBCursor snapshot = sequoiadb.getSnapshot(Sequoiadb.SDB_SNAP_CATALOG, "{\"Name\":\"bar.maincl\"}", null, null);
        while (snapshot.hasNext()) {
            BSONObject next = snapshot.getNext();
            System.out.println(next);
        }
    }

    public DBCollection getCollection() {
        //获得连接
        Sequoiadb sequoiadb = getConnection();
        //获取foo集合空间
        CollectionSpace cs = sequoiadb.getCollectionSpace("foo");
        //获取bar集合
        DBCollection cl = cs.getCollection("bar");
        return cl;
    }

    public void insert() {
        DBCollection dbCollection = getCollection();
        BSONObject bsonObject = new BasicBSONObject();
        //在此对象中设置键值对
        bsonObject.put("name", "jerry");
        bsonObject.put("age", 22);
        /**
         * 向当前集合中插入一条记录，如果记录中不包含字段"_id"，将会自动添加。
         * @param insertor 要插入的BSON对象，不能为null
         * @return 表示"_id"值的对象
         */
        Object oid = dbCollection.insert(bsonObject);
        System.out.println(oid);
    }

    public void bulkinsert() {
        DBCollection dbCollection = getCollection();
        List<BSONObject> bsonObjects = new ArrayList<>();
        bsonObjects.add(new BasicBSONObject("name", "rose"));
        bsonObjects.add(new BasicBSONObject("name", "jack"));
        bsonObjects.add(new BasicBSONObject("name", "tom"));
        // bsonObjects.add...
        /**
         * 向当前集合中批量插入数据
         * @param insertor 要插入的BSON对象集合，不能为null
         * @param flag 可选值为 0 或 1。
         * 			如果为1，当发生主键重复时此操作会继续执行（主键重复数据将被忽略），
         *	可用常量DBCollection.FLG_INSERT_CONTONDUP表示；
         *			如果为0，当发生主键重复时此操作被中断。
         */
        dbCollection.bulkInsert(bsonObjects, 0);
    }

    //获取当前集合全部记录
    public void query() {
        DBCollection dbCollection = getCollection();
        // 查询所有记录，并把查询结果放在游标对象中
        DBCursor cursor = dbCollection.query();
        try {
            while (cursor.hasNext()) {
                BSONObject record = cursor.getNext();
                // 可将返回记录映射成自定义实体对象(Person)
//                Person person = record.as(Person.class);
//                System.out.println(person.toString());
            }
        } catch (BaseException e) {
            System.out.println("Sequoiadb driver error, error description:" + e.getErrorType());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
    }

    public void queryByBsonObj() {
        DBCollection dbCollection = getCollection();
        // 查询条件，查询年龄小于20岁的
        BasicBSONObject matcher = new BasicBSONObject("age", new BasicBSONObject("$lt", 20));
        // 返回字段，这里指定返回"name"和"age"
        BasicBSONObject selector = new BasicBSONObject();
        selector.put("name", "");
        selector.put("age", "");
        // 排序规则，1代表升序，-1代表降序
        // 注意1：如果不设定排序内容，则对返回的结果集不排序。
        // 注意2：如果指定排序字段不包含在返回字段（selector）中，此时设置的排序无意思，将被自动忽略。
        BasicBSONObject orderBy = new BasicBSONObject("age", 1);
        // 指定使用的索引
        BasicBSONObject hint = new BasicBSONObject("", "ageIndex");
        /**
         * 获取当前集合中匹配条件的记录
         * @param matcher 匹配条件，如果为null则返回所有记录
         * @param selector 返回字段，如果为null则返回所有字段
         * @param orderBy 排序规则，如果为null则不进行排序
         * @param hint 指定使用的索引，如：
         *        {"": "ageIndex"}，使用名为'ageIndex'的索引来访问数据（index scan）;
         *        {"":null}不使用索引来访问数据（table scan）。
         *        如果为null，则自动匹配最佳的索引来访问数据。
         * @return 表示结果集的游标实例
         */
        DBCursor cursor = dbCollection.query(matcher, selector, orderBy, hint);

        try {
            while (cursor.hasNext()) {
                BSONObject record = cursor.getNext();
                System.out.println(record.get("Name") + ":" + record.get("age"));
            }
        } catch (BaseException e) {
            System.out.println("Sequoiadb driver error, error description:" + e.getErrorType());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
    }

    public void queryByDBQuery() {
        DBCollection dbCollection = getCollection();

        // 查询条件，查询年龄小于20岁的
        BasicBSONObject matcher = new BasicBSONObject("age", new BasicBSONObject("$lt", 20));
        // 返回字段，这里指定返回"name"和"age"
        BasicBSONObject selector = new BasicBSONObject();
        selector.put("name", "");
        selector.put("age", "");
        // 排序规则，1代表升序，-1代表降序
        // 注意1：如果不设定排序内容，则对返回的结果集不排序。
        // 注意2：如果指定排序字段不包含在返回字段（selector）中，此时设置的排序无意思，将被自动忽略。
        BasicBSONObject orderBy = new BasicBSONObject("age", 1);
        // 指定使用的索引
        BasicBSONObject hint = new BasicBSONObject("", "ageIndex");

        // 封装DBQuery对象
        DBQuery dbQuery = new DBQuery();
        dbQuery.setMatcher(matcher);
        dbQuery.setSelector(selector);
        dbQuery.setOrderBy(orderBy);
        dbQuery.setHint(hint);

        DBCursor cursor = dbCollection.query(dbQuery);

        try {
            while (cursor.hasNext()) {
                BSONObject record = cursor.getNext();
                System.out.println(record.get("Name") + ":" + record.get("age"));
            }
        } catch (BaseException e) {
            System.out.println("Sequoiadb driver error, error description:" + e.getErrorType());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
    }

    public void queryByString() {
        DBCollection dbCollection = getCollection();

        // 查询条件，查询年龄小于20岁的
        String matcher = "{age: {$lt:20}}";
        // 返回字段，这里指定返回"name"和"age"
        String selector = "{name:\"\", age:\"\"}";
        // 排序规则，1代表升序，-1代表降序
        // 注意1：如果不设定排序内容，则对返回的结果集不排序。
        // 注意2：如果指定排序字段不包含在返回字段（selector）中，此时设置的排序无意思，将被自动忽略。
        String orderBy = "{age:1}";
        // 指定使用的索引
        String hint = "{\"\": \"ageIndex\"}";

    /*
        此方法省去了构造BSONObject对象的麻烦，
        内部使用 JSON.parse(String s) 将传递的表达式解析成了BSONObject对象
     */
        DBCursor cursor = dbCollection.query(matcher, selector, orderBy, hint);

        try {
            while (cursor.hasNext()) {
                BSONObject record = cursor.getNext();
                System.out.println(record.get("Name") + ":" + record.get("age"));
            }
        } catch (BaseException e) {
            System.out.println("Sequoiadb driver error, error description:" + e.getErrorType());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
    }

    /*
     * 与条件查询类似,只需额外指定两个long类型的参数分别表示"跳过条数"与"返回条数".
     * 本例使用BSONObject的方式,其它方式参考"条件查询"一节
     * skipRows 从返回结果集的第一条开始,指定跳过多少条.如果指定为0,则不跳过.
     * returnRows 返回指定数量的记录.如果指定为0,返回空;-1,返回全部.
     */
    public void queryByPage() {
//        GetCS cs = new GetCS();
//        DBCollection dbCollection = cs.getCollection();
        DBCollection dbCollection = null;
        //查询条件,查询年龄小于20岁的
        BasicBSONObject matcher = new BasicBSONObject("age", new BasicBSONObject("$lt", 20));
        // 返回字段，这里指定返回"name"和"age"
        BasicBSONObject selector = new BasicBSONObject();
        selector.put("name", "");
        selector.put("age", "");
        // 排序规则，1代表升序，-1代表降序
        // 注意1：如果不设定排序内容，则对返回的结果集不排序。
        // 注意2：如果指定排序字段不包含在返回字段（selector）中，此时设置的排序无意思，将被自动忽略。
        BasicBSONObject orderBy = new BasicBSONObject("age", 1);
        // 指定使用的索引，这里指定null，自动匹配
        BasicBSONObject hint = null;
        //获得匹配的记录的条数
        long count = dbCollection.getCount(matcher);
        //每页返回条数
        long returnRows = 10;
        //总页数
        long pages = count % returnRows == 0 ? count / returnRows : count / returnRows + 1;
        for (int i = 0; i < pages; i++) {
            long skipRows = i * returnRows;
            DBCursor cursor = dbCollection.query(matcher, selector, orderBy, hint, skipRows, returnRows);

            try {
                System.out.println("page" + i);
                while (cursor.hasNext()) {
                    BSONObject record = cursor.getNext();
                    System.out.println(record.get("Name") + ":" + record.get("age"));
                }
            } catch (BaseException e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
        }
    }

    /**
     * 更新集合中所有记录，使用 $inc 将 age 字段的值增加1，不设定matcher和hint参数内容
     */
    public void update1() {
        DBCollection dbCollection = getCollection();
        BSONObject modifier = new BasicBSONObject();
        modifier.put("$inc", new BasicBSONObject("age", 1));
        //@param matcher 匹配条件
        //@param modifier 更新规则
        //@param hint 指定使用的索引
        dbCollection.update(null, modifier, null);
    }

    public void update2() {
        DBCollection dbCollection = getCollection();
        BSONObject matcher = new BasicBSONObject();
        matcher.put("name", new BasicBSONObject("$exsit", 1));
        matcher.put("age", new BasicBSONObject("$exsit", 0));
        BSONObject modifier = new BasicBSONObject();
        modifier.put("$unset", new BasicBSONObject("age", ""));
        dbCollection.update(matcher, modifier, null);
    }

    public void delete() {
        DBCollection dbCollection = getCollection();
        BSONObject matcher = new BasicBSONObject();
        //删除年龄大于20的记录，使用名为“testIndex"的索引
        matcher.put("age", new BasicBSONObject("$gt", 20));
        BSONObject hint = new BasicBSONObject("", "testIndex");
        //@param matcher 匹配条件
        //@param hint 指定使用的索引
        dbCollection.delete(matcher, hint);
    }

    public void truncate() {
        DBCollection dbCollection = getCollection();
        dbCollection.truncate();
    }


}
