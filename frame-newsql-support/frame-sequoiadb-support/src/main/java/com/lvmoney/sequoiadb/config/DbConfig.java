package com.lvmoney.sequoiadb.config;/**
 * 描述:
 * 包名:com.lvmoney.sequoiadb.config
 * 版本信息: 版本1.0
 * 日期:2019/11/5
 * Copyright XXXXXX科技有限公司
 */


import com.sequoiadb.base.ConfigOptions;
import com.sequoiadb.base.Sequoiadb;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/5 17:19
 */
@Configuration
public class DbConfig {
    public Sequoiadb getConnection() {
        ConfigOptions configOptions = new ConfigOptions();
        List<String> connStrings = new ArrayList<String>();
        connStrings.add("192.168.248.128:11810");
        connStrings.add("192.168.248.129:11810");
        connStrings.add("192.168.248.130:11810");
        /**
         * @param connStrings 远程服务器地址"主机:端口"(集合类型)
         * @param username 用户名
         * @param password 密码
         * @param configOptions 数据库连接配置选项
         */
        Sequoiadb sequoiadb = new Sequoiadb(connStrings, "sdbadmin", "sdbadmin", configOptions);
        return sequoiadb;
    }
}
