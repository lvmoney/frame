package com.lvmoney.sequoiadb.config;/**
 * 描述:
 * 包名:com.lvmoney.sequoiadb.config
 * 版本信息: 版本1.0
 * 日期:2019/11/5
 * Copyright XXXXXX科技有限公司
 */


import com.sequoiadb.base.ConfigOptions;
import com.sequoiadb.base.Sequoiadb;
import com.sequoiadb.datasource.ConnectStrategy;
import com.sequoiadb.datasource.DatasourceOptions;
import com.sequoiadb.datasource.SequoiadbDatasource;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/5 17:23
 */
@Configuration
public class DbPool {
    public static void main(String[] args) throws InterruptedException {
        //创建集合存储服务器地址
        ArrayList<String> addrs = new ArrayList<String>();
        addrs.add("192.168.248.128:11810");
        addrs.add("192.168.248.129:11810");
        addrs.add("192.168.248.130:11810");
        String user = "sdbadmin";
        String password = "sdbadmin";
        //创建连接配置实例
        ConfigOptions nwOpt = new ConfigOptions();
        //创建连接池配置实例
        DatasourceOptions dsOpt = new DatasourceOptions();
        //创建连接池实例
        SequoiadbDatasource ds = null;

        /* 设置网络参数 */
        // 建连超时时间为500ms
        nwOpt.setConnectTimeout(500);
        // 建连失败后重试时间为0ms
        nwOpt.setMaxAutoConnectRetryTime(0);

        /* 设置连接池参数 */
        // 连接池最多能提供500个连接
        dsOpt.setMaxCount(500);
        // 每次增加20个连接
        dsOpt.setDeltaIncCount(20);
        // 连接池空闲时，保留20个连接
        dsOpt.setMaxIdleCount(20);
        // 池中空闲连接存活时间。单位:毫秒。 0表示不关心连接隔多长时间没有收发消息
        dsOpt.setKeepAliveTimeout(0);
        // 每隔60秒将连接池中多于MaxIdleCount限定的空闲连接关闭，并将存活时间过长（连接已停止收发超过keepAliveTimeout时间）的连接关闭。
        dsOpt.setCheckInterval(60 * 1000);
        // 向catalog同步coord地址的周期。单位:毫秒。0表示不同步
        dsOpt.setSyncCoordInterval(0);
        // 连接出池时，是否检测连接的可用性，默认不检测
        dsOpt.setValidateConnection(false);
        // 默认使用coord地址负载均衡的策略获取连接
        dsOpt.setConnectStrategy(ConnectStrategy.BALANCE);

        // 建立连接池
        ds = new SequoiadbDatasource(addrs, user, password, nwOpt, dsOpt);
        // 从连接池获取连接
        Sequoiadb sequoiadb = null;
        try {
            sequoiadb = ds.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (sequoiadb != null) {
                // 将连接归还连接池
                ds.releaseConnection(sequoiadb);
            }
        }
        // 任务结束后，关闭连接池
        ds.close();
    }
}
