package com.lvmoney.mysql.base.service;/**
 * 描述:
 * 包名:com.lvmoney.platform.member.info.service
 * 版本信息: 版本1.0
 * 日期:2020/1/20
 * Copyright XXXXXX科技有限公司
 */


/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2020/1/20 16:53
 */
public interface UidService {
    long getUid();
}
