package com.lvmoney.mysql.subdb.po;/**
 * 描述:
 * 包名:com.zhy.platform.dictionaries.search.entity
 * 版本信息: 版本1.0
 * 日期:2020/1/3
 * Copyright XXXXXX科技有限公司
 */


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2020/1/3 11:10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DictionariesInfo {
    /**
     * 权限id
     */
    private String id;

    /**
     * 名称
     */
    private String name;
    /**
     * code
     */
    private String code;

}
