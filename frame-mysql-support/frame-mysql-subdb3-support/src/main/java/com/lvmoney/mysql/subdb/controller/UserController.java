package com.lvmoney.mysql.subdb.controller;/**
 * 描述:
 * 包名:com.lvmoney.mysql.subdb.controller
 * 版本信息: 版本1.0
 * 日期:2019/9/10
 * Copyright XXXXXX科技有限公司
 */


import com.alibaba.druid.support.json.JSONUtils;
import com.lvmoney.mysql.subdb.po.Order;
import com.lvmoney.mysql.subdb.po.OrderItem;
import com.lvmoney.mysql.subdb.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/9/10 15:35
 */
@RestController
public class UserController {
    @Autowired
    private OrderService orderService;

    @GetMapping("/user/save")
    @ResponseBody
    public void save() {
        for (int i = 0; i < 10; i++) {
            Integer orderId = 1000 + i;
            Integer userId = 10 + i;

            Order o = new Order();
            o.setOrderId(orderId);
            o.setUserId(userId);
            o.setConfigId(i);
            o.setRemark("save.order");
            orderService.saveOrder(o);

            OrderItem oi = new OrderItem();
            oi.setOrderId(orderId);
            oi.setRemark("save.orderItem");
            orderService.saveOrderItem(oi, userId);
        }
    }

    @GetMapping("/user/list")
    @ResponseBody
    public String list(int userId, int orderId) {
        return orderService.selectBySharding(userId, orderId).toString();
    }
}
