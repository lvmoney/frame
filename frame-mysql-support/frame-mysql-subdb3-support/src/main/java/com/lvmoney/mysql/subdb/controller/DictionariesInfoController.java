package com.lvmoney.mysql.subdb.controller;/**
 * 描述:
 * 包名:com.lvmoney.mysql.subdb.controller
 * 版本信息: 版本1.0
 * 日期:2020/1/7
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.mysql.subdb.po.DictionariesInfo;
import com.lvmoney.mysql.subdb.service.DictionariesInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2020/1/7 10:02
 */
@RestController
public class DictionariesInfoController {

    @Autowired
    private DictionariesInfoService dictionariesInfoService;

    @GetMapping("/dic/save")
    @ResponseBody
    public void save() {
        for (int i = 0; i < 10; i++) {
            DictionariesInfo dictionariesInfo = new DictionariesInfo();
            dictionariesInfo.setId(String.valueOf(100 + i));
            dictionariesInfo.setCode("A" + String.valueOf(100 + i));
            dictionariesInfo.setName("B" + String.valueOf(100 + i));
            dictionariesInfoService.saveDictionariesInfo(dictionariesInfo);
        }
    }
}
