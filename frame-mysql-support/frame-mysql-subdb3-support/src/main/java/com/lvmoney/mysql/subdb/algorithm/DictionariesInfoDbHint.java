package com.lvmoney.mysql.subdb.algorithm;/**
 * 描述:
 * 包名:com.lvmoney.mysql.subdb.algorithm
 * 版本信息: 版本1.0
 * 日期:2020/1/7
 * Copyright XXXXXX科技有限公司
 */


import org.apache.shardingsphere.api.sharding.hint.HintShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.hint.HintShardingValue;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2020/1/7 9:59
 */
public class DictionariesInfoDbHint implements HintShardingAlgorithm<Integer> {
    /**
     * @param availableTargetNames 分片表名的集合
     * @param hintShardingValue    分片键集合
     * @return java.util.Collection<java.lang.String>
     * @author hujy
     * @date 2019-09-22 12:23
     */
    @Override
    public Collection<String> doSharding(Collection<String> availableTargetNames, HintShardingValue<Integer> hintShardingValue) {
        Collection<String> result = new ArrayList<>();
        for (String each : availableTargetNames) {
            for (Integer value : hintShardingValue.getValues()) {
                if (each.endsWith(String.valueOf(value % 2))) {
                    System.out.println("*********************");
                    result.add(each);
                }
            }
        }
        return result;
    }
}
