package com.lvmoney.mysql.subdb.service;/**
 * 描述:
 * 包名:com.lvmoney.mysql.subdb.service
 * 版本信息: 版本1.0
 * 日期:2020/1/7
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.mysql.subdb.po.DictionariesInfo;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2020/1/7 10:00
 */
public interface DictionariesInfoService {
    Integer saveDictionariesInfo(DictionariesInfo dictionariesInfo);
}
