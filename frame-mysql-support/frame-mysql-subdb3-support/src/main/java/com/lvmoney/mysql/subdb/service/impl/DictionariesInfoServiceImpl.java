package com.lvmoney.mysql.subdb.service.impl;/**
 * 描述:
 * 包名:com.lvmoney.mysql.subdb.service.impl
 * 版本信息: 版本1.0
 * 日期:2020/1/7
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.mysql.subdb.dao.DictionariesInfoDao;
import com.lvmoney.mysql.subdb.po.DictionariesInfo;
import com.lvmoney.mysql.subdb.service.DictionariesInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2020/1/7 10:01
 */
@Service
public class DictionariesInfoServiceImpl implements DictionariesInfoService {
    @Autowired
    DictionariesInfoDao dictionariesInfoDao;

    @Override
    public Integer saveDictionariesInfo(DictionariesInfo dictionariesInfo) {
        return dictionariesInfoDao.save(dictionariesInfo);
    }
}
