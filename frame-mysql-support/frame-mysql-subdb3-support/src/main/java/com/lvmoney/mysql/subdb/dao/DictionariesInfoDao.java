package com.lvmoney.mysql.subdb.dao;/**
 * 描述:
 * 包名:com.lvmoney.mysql.subdb.dao
 * 版本信息: 版本1.0
 * 日期:2020/1/7
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.mysql.subdb.po.DictionariesInfo;
import org.apache.ibatis.annotations.Insert;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2020/1/7 9:55
 */
public interface DictionariesInfoDao {
    @Insert("insert into member_info(id,name,code) values(#{id},#{name},#{code})")
    Integer save(DictionariesInfo dictionariesInfo);
}
