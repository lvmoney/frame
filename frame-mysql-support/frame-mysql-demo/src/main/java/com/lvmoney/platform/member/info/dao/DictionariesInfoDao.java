package com.lvmoney.platform.member.info.dao;

import com.lvmoney.platform.member.info.entity.DictionariesInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会用相关字典表 Mapper 接口
 * </p>
 *
 * @author lvmoney
 * @since 2020-01-15
 */
public interface DictionariesInfoDao extends BaseMapper<DictionariesInfo> {

}
