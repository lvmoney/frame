package com.lvmoney.platform.member.info.config;/**
 * 描述:
 * 包名:com.lvmoney.mysql.base.config
 * 版本信息: 版本1.0
 * 日期:2020/1/20
 * Copyright XXXXXX科技有限公司
 */


import com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator;
import com.lvmoney.mysql.base.service.UidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2020/1/20 11:26
 */
@Component
public class CustomIdGenerator extends DefaultIdentifierGenerator {
    @Autowired
    UidService uidService;

    @Override
    public Long nextId(Object entity) {
        //可以将当前传入的class全类名来作为bizKey,或者提取参数来生成bizKey进行分布式Id调用生成.
//        String bizKey = entity.getClass().getName();
        //使用百度提供的主键生成工具
        long id = uidService.getUid();
        //返回生成的id值即可.
        return id;
    }
}
