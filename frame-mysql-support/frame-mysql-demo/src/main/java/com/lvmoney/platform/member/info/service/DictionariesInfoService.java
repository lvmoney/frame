package com.lvmoney.platform.member.info.service;

import com.lvmoney.platform.member.info.entity.DictionariesInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会用相关字典表 服务类
 * </p>
 *
 * @author lvmoney
 * @since 2020-01-15
 */
public interface DictionariesInfoService extends IService<DictionariesInfo> {

}
