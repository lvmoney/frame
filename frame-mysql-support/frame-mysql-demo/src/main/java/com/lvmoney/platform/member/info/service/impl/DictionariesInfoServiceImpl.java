package com.lvmoney.platform.member.info.service.impl;

import com.lvmoney.platform.member.info.dao.DictionariesInfoDao;
import com.lvmoney.platform.member.info.entity.DictionariesInfo;
import com.lvmoney.platform.member.info.service.DictionariesInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会用相关字典表 服务实现类
 * </p>
 *
 * @author lvmoney
 * @since 2020-01-15
 */
@Service
public class DictionariesInfoServiceImpl extends ServiceImpl<DictionariesInfoDao, DictionariesInfo> implements DictionariesInfoService {

}
