package com.lvmoney.common.util;/**
 * 描述:
 * 包名:com.lvmoney.common.util
 * 版本信息: 版本1.0
 * 日期:2019/11/6
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.common.util.bloomfilter.BloomFilter1;

import java.util.ArrayList;
import java.util.List;


/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/6 17:47
 */
public class TestClass {
    public static void main(String[] args) {
        List<Integer> inint2 = new ArrayList() {{
            add(48);
            add(-1);
            add(-1);
            add(-1);
            add(-1);
            add(-1);
        }};
        inint2 = AsciiUtil.getPrex(inint2);
        String prex = AsciiUtil.getPrexString(inint2);
        //初次测试需要打开
//        BloomFilter1 fileter = new BloomFilter1(100000000);
        //后面直接从文件中获得
        String fileterPath = "D:\\data\\temp\\test.txt";
        BloomFilter1 fileter = BloomFilter1.readFilterFromFile(fileterPath);
        List<String> list = new ArrayList();
        for (int ii = 0; ii < 1000; ii++) {
            for (int i = 0; i < 10000; i++) {
                list.add(getNotExistString(prex, fileter));
                fileter.addIfNotExist(prex);
            }
            System.out.println("for:" + ii);
        }
        List<String> list2 = new ArrayList();
        for (int i = 0; i < 1000; i++) {
            list2.add(getNotExistString(prex, fileter));
            fileter.addIfNotExist(prex);
        }
        System.out.println("result:" + list2);
        fileter.saveFilterToFile(fileterPath);
    }

    public static String getNotExistString(String prex, BloomFilter1 filter) {
        while (true) {
            int lenght = 6 - prex.length();
            String sub = AsciiUtil.getSpecifiedLengthString(lenght);
            String res = prex + sub;
            if (filter.addIfNotExist(res)) {
                return res;
            }
        }


    }
}
