/**
 * 描述:
 * 包名:com.scltzhy.pay.utils
 * 版本信息: 版本1.0
 * 日期:2018年10月11日  上午10:02:58
 * Copyright XXXXXX科技有限公司
 */

package com.lvmoney.common.util;


import com.lvmoney.common.constant.CommonConstant;
import com.lvmoney.common.util.vo.ReflectVo;

/**
 * @describe：
 * @author: lvmoney /XXXXXX科技有限公司
 * @version:v1.0 2018年10月11日 上午10:02:58
 */

public class SignUtil {
    /**
     * 泛型实体椭圆曲线私钥签名
     *
     * @param t:         实体
     * @param publicKey: 公钥
     * @throws
     * @return: java.lang.String
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/11/6 17:20
     */
    public static <T> String t2Sign(T t, String publicKey) {
        String res = getPlianText(t);
        String sign = EcdsaUtil.getEcdsaSign(res, CommonConstant.EEC_FACTORY_TYPE, publicKey, CommonConstant.EEC_SIGNATURE_TYPE);
        return sign;
    }

    /**
     * 椭圆曲线公钥校验签名
     *
     * @param res:       原文
     * @param sign:      密文
     * @param publicKey: 公钥
     * @throws
     * @return: boolean
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/11/6 17:20
     */
    public static boolean verifySign(String res, String sign, String publicKey) {
        boolean bool = false;
        bool = EcdsaUtil.verifyEcdsa(res, sign, CommonConstant.EEC_FACTORY_TYPE, publicKey, CommonConstant.EEC_SIGNATURE_TYPE);
        return bool;
    }

    /**
     * 获得泛型实体需要签名的明文
     *
     * @param t: 实体vo对象
     * @throws
     * @return: java.lang.String
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/11/6 17:21
     */
    public static <T> String getPlianText(T t) {
        ReflectVo<T> reflectVo = new ReflectVo<>();
        reflectVo.setKeyToUpper(CommonConstant.API_TO_UPPER);
        reflectVo.setUrlEncode(CommonConstant.API_URL_ENCODE);
        reflectVo.setData(t);
        String res = ReflectUtil.getContent(reflectVo).getContent();
        return res;
    }
}
