package com.lvmoney.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

/**
 * 生成各种hash值得工具类
 *
 * @author thunder
 */
public class HashUtil {

    /**
     * 获得文件CRC32。当文件大小在100kb以内，获取速度还可以，文件大了后相当慢
     *
     * @param filePath:
     * @throws
     * @return: java.lang.String
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/11/6 17:41
     */
    public static String getFileCRC32(String filePath) {
        CRC32 crc32 = new CRC32();
        FileInputStream inputStream = null;
        CheckedInputStream checkedinputstream = null;
        String crcStr = null;
        try {
            inputStream = new FileInputStream(new File(filePath));
            checkedinputstream = new CheckedInputStream(inputStream, crc32);
            while (checkedinputstream.read() != -1) {
            }
            crcStr = Long.toHexString(crc32.getValue()).toUpperCase();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            if (checkedinputstream != null) {
                try {
                    checkedinputstream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return crcStr;
    }

    /**
     * 获得文件的各种hash值。当文件达到700m左右，大概需要3000ms。文件数据量小的时候速度非常快
     *
     * @param filePath:
     * @param hashType:
     * @throws
     * @return: java.lang.String
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/11/6 17:41
     */
    public static String getFileHash(String filePath, String hashType) {
        File file = new File(filePath);
        if (file == null || !file.exists()) {
            return null;
        }
        String result = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            MappedByteBuffer mbf = fis.getChannel().map(
                    FileChannel.MapMode.READ_ONLY, 0, file.length());
            MessageDigest md = MessageDigest.getInstance(hashType);
            md.update(mbf);
            BigInteger bi = new BigInteger(1, md.digest());
            result = bi.toString(16);
        } catch (Exception e) {
            return null;
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.getStackTrace();
                }
            }
        }
        return result;
    }

    /**
     * 获取字符串的各种hash值
     *
     * @param source:
     * @param hashType:
     * @throws
     * @return: java.lang.String
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/11/6 17:42
     */
    public static String getStringHash(String source, String hashType) {
        // 返回值
        String _result = null;

        // 是否是有效字符串
        if (source != null && source.length() > 0) {
            try {
                // SHA 加密开始
                // 创建加密对象 并傳入加密類型
                MessageDigest messageDigest = MessageDigest.getInstance(hashType);
                // 传入要加密的字符串
                messageDigest.update(source.getBytes());
                // 得到 byte 類型结果
                byte byteBuffer[] = messageDigest.digest();

                // 將 byte 轉換爲 string
                StringBuffer strHexString = new StringBuffer();
                // 遍歷 byte buffer
                for (int i = 0; i < byteBuffer.length; i++) {
                    String hex = Integer.toHexString(0xff & byteBuffer[i]);
                    if (hex.length() == 1) {
                        strHexString.append('0');
                    }
                    strHexString.append(hex);
                }
                // 得到返回結果
                _result = strHexString.toString();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        return _result;
    }

    /**
     * 获取字符串的CRC32
     *
     * @param source:
     * @throws
     * @return: java.lang.String
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/11/6 17:42
     */
    public static String getStringCRC32(String source) {
        String _result = "";
        CRC32 crc32 = new CRC32();
        crc32.update(source.getBytes());
        _result = Long.toHexString(crc32.getValue());
        return _result;
    }


    public static byte[] sha1Hash(String toHash) {
        /* Create a MessageDigest */
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-1");
            /* Add password bytes to digest */
            md.update(toHash.getBytes());

            /* Get the hashed bytes */
            return md.digest();
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            return null;
        }


    }

    public static void main(String[] args) {
        String str = HashUtil.getStringHash("test", "SHA-512");
        System.out.println(str);
        System.out.println(str.length());
        byte[] a = HashUtil.sha1Hash("a");
        byte[] b = HashUtil.sha1Hash("b");
        System.out.println();
    }
}
