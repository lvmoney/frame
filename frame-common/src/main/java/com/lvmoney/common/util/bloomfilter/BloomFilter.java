package com.lvmoney.common.util.bloomfilter;


import com.lvmoney.common.constant.CommonConstant;
import com.lvmoney.common.util.HashUtil;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * 1、uxto的数据存储为一行一条数据，不保存为实体
 * 2、bloom保存实体对象到文件
 * 3、放在内存中，如果没有就重新生成一次bloom实体文件
 *
 * @param <E>
 * @author thunder
 */
public class BloomFilter<E> implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -5435922713708704873L;
    private BitSet bs;
    private int bitArraySize = 100000000;
    private int numHashFunc = 6;

    public BloomFilter() {
        bs = new BitSet(bitArraySize);
    }

    public void add(E obj) {
        int[] indexes = getHashIndexes(obj);
        for (int index : indexes) {
            bs.set(index);
        }
    }

    public boolean contains(E obj) {

        int[] indexes = getHashIndexes(obj);
        for (int index : indexes) {
            if (bs.get(index) == false) {
                return false;
            }
        }
        return true;
    }

    public void union(BloomFilter<E> other) {
        bs.or(other.bs);
    }

    /* 粗略实现，采用MD5散列作为java随机数生成器的种子并取k个随机数作为索引 */
    public int[] getHashIndexes(E obj) {
        int[] indexes = new int[numHashFunc];
        long seed = 0;
        byte[] digest;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(obj.toString().getBytes());
            digest = md.digest();
            for (int i = 0; i < 6; i++) {
                seed = seed ^ (((long) (digest[i] & 0xFF)) << (8 * i));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        Random gen = new Random(seed);
        for (int i = 0; i < numHashFunc; i++) {
            indexes[i] = gen.nextInt(bitArraySize);
        }
        return indexes;
    }


    /**
     * 读取uxto的存储文件并生成map对象，map的key值：行数据的hash
     *
     * @param filePath
     * @return
     */
    public Map<String, String> readUxtoFile(String filePath) {
        BufferedReader br;
        Map<String, String> map = new HashMap<String, String>();
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                String key = HashUtil.getStringHash(line, CommonConstant.UTXO_MAP_KEY_HASH_TYPE);
                map.put(key, line);
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }


    // public void write(DataOutput out) throws IOException {
    // int byteArraySize = (int) (bitArraySize / 8);
    // byte[] byteArray = new byte[byteArraySize];
    // for (int i = 0; i < byteArraySize; i++) {
    // byte nextElement = 0;
    // for (int j = 0; j < 8; j++) {
    // if (bs.get(8 * i + j))
    // nextElement |= 1 << j;
    // }
    // byteArray[i] = nextElement;
    // }
    // out.write(byteArray);
    // }
    //
    // public void readFileds(DataInput in) throws IOException {
    // int byteArraySize = (int) (bitArraySize / 8);
    // byte[] byteArray = new byte[byteArraySize];
    // in.readFully(byteArray);
    // for (int i = 0; i < byteArraySize; i++) {
    // byte nextByte = byteArray[i];
    // for (int j = 0; j < 8; j++) {
    // if (((int) nextByte & (1 << j)) != 0)
    // bs.set(8 * i + j);
    // }
    // }
    // }
    //
    // public Map<Integer, String> readFile(String filePath) {
    // BufferedReader br;
    // Map<Integer, String> map = new HashMap<Integer, String>();
    // try {
    // br = new BufferedReader(new InputStreamReader(new
    // FileInputStream(filePath)));
    // int i = 0;
    // for (String line = br.readLine(); line != null; line = br.readLine()) {
    // map.put(i++, line);
    // }
    // br.close();
    // } catch (FileNotFoundException e) {
    // e.printStackTrace();
    // } catch (IOException e) {
    // e.printStackTrace();
    // }
    // return map;
    // }
    //
    //

    public static void main(String[] args) {

        //System.out.println(uxtoL1.get(key));
/*BloomFilter<String> bf = new BloomFilter<String>();
long startTime8 = System.currentTimeMillis();
  Map<Integer, String> map = FileUtil.readFile("F:\\Temp\\test.txt");
  System.out.println("map size=" + map.size());
  long endTime8 = System.currentTimeMillis(); // 获取结束时间
	System.out.println("读map文件程序运行时间：" + (endTime8 - startTime8) + "ms");
	
	long startTime9 = System.currentTimeMillis();
		for (Map.Entry<Integer, String> m : map.entrySet())
			bf.add(m.getValue());
		
		  long endTime9 = System.currentTimeMillis(); // 获取结束时间
			System.out.println("写入bf文件程序运行时间：" + (endTime9 - startTime9) + "ms");
		long startTime1 = System.currentTimeMillis();
		FileUtil.saveObjectToFile("F:\\\\Temp\\\\test2.txt", bf);
		long endTime1 = System.currentTimeMillis(); // 获取结束时间
		System.out.println("保存到文件程序运行时间：" + (endTime1 - startTime1) + "ms");
		long startTime = System.currentTimeMillis();
		boolean flag = bf.contains("我是陈曦");
		long endTime = System.currentTimeMillis(); // 获取结束时间
		System.out.println("程序运行时间：" + (endTime - startTime) + "ms");

		long startTime3 = System.currentTimeMillis();

		// BloomFilter<String> bf2 = bf.readFilterFromFile("F:\\\\Temp\\\\test2.txt");

		BloomFilter<String> bf3 = FileUtil.readObjectFromFile("F:\\\\Temp\\\\test2.txt");
		boolean flag3 = bf3.contains("我是许莽林");
		BitSet bbs = bf3.getBs();
		System.out.println("bbs:"+bbs.get(0));
		long endTime3 = System.currentTimeMillis(); // 获取结束时间
		System.out.println("大数据测试:" + flag3);
		System.out.println("从文件中读foolter程序运行时间：" + (endTime3 - startTime3) + "ms");*/

        BloomFilter<String> bf = new BloomFilter<String>();
        long startTime8 = System.currentTimeMillis();
        Map<String, String> map = bf.readUxtoFile("E:\\Temp\\test9.txt");
        long endTime8 = System.currentTimeMillis(); // 获取结束时间
        System.out.println(map.get("0ea6e48dde03e6af5571616e977cba4dedbf2de956281a038bdea1878cacb32910000"));
        System.out.println("从文件中读foolter程序运行时间：" + (endTime8 - startTime8) + "ms");

    }

    public BitSet getBs() {
        return bs;
    }

    public void setBs(BitSet bs) {
        this.bs = bs;
    }

    public int getBitArraySize() {
        return bitArraySize;
    }

    public void setBitArraySize(int bitArraySize) {
        this.bitArraySize = bitArraySize;
    }

    public int getNumHashFunc() {
        return numHashFunc;
    }

    public void setNumHashFunc(int numHashFunc) {
        this.numHashFunc = numHashFunc;
    }
}
