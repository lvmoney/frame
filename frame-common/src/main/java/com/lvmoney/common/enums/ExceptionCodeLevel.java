package com.lvmoney.common.enums;/**
 * 描述:
 * 包名:com.lvmoney.common.enums
 * 版本信息: 版本1.0
 * 日期:2019/11/19
 * Copyright XXXXXX科技有限公司
 */


/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/19 16:25
 */
public enum ExceptionCodeLevel {
    /**
     * 权限模块错误码范围
     */
    AUTHENTICATION(5000),
    /**
     * 缓存错误码范围
     */
    CACHE(6000),
    /**
     * 配置中心错误码范围
     */
    CONFIGSERVER(7000),
    /**
     * 基础模块错误码范围
     */
    CORE(8000),
    /**
     * 调度器错误码范围
     */
    JOB(9000),
    /**
     * 日志错误码范围
     */
    LOG(1000),
    /**
     * 消息队列错误码范围
     */
    MQ(3000),


    ;

    private Integer value;

    ExceptionCodeLevel(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
