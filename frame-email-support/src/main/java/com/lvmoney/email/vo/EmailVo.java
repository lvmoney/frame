package com.lvmoney.email.vo;/**
 * 描述:
 * 包名:com.lvmoney.email.vo
 * 版本信息: 版本1.0
 * 日期:2019/11/22
 * Copyright XXXXXX科技有限公司
 */


import lombok.Data;

import java.io.Serializable;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/22 11:41
 */
@Data
public class EmailVo implements Serializable {
    private String from;
    private String to;
    private String title;
    private String context;
}
