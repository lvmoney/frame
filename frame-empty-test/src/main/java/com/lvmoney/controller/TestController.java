package com.lvmoney.controller;/**
 * 描述:
 * 包名:com.lvmoney.mysql.subdb.controller
 * 版本信息: 版本1.0
 * 日期:2019/9/10
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.common.exception.BusinessException;
import com.lvmoney.common.exception.CommonException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/9/10 15:35
 */
@RestController
public class TestController {

    @GetMapping("/error/test")
    public void save() {
        throw new BusinessException(CommonException.Proxy.REDIS_KEY_IS_REQUIRED);
    }

}
