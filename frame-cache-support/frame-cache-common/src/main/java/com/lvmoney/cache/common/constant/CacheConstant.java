package com.lvmoney.cache.common.constant;/**
 * 描述:
 * 包名:com.lvmoney.cache.common.constant
 * 版本信息: 版本1.0
 * 日期:2019/11/20
 * Copyright XXXXXX科技有限公司
 */


/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/20 11:10
 */
public class CacheConstant {
    /**
     * rediscache
     */
    public static final String CACHE_REDIS = "redisCache";

    /**
     * rediscache
     */
    public static final String CACHE_MONGO = "mongoCache";
}
