package com.lvmoney.rabbitmq.ack.annotation;/**
 * 描述:
 * 包名:com.lvmoney.rabbitmq.ack.annotation
 * 版本信息: 版本1.0
 * 日期:2019/11/12
 * Copyright XXXXXX科技有限公司
 */


import java.lang.annotation.*;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/12 18:29
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface DyProviderService {
    String name() default "";
}
