package com.lvmoney.rabbitmq.ack.customer;/**
 * 描述:
 * 包名:com.lvmoney.rabbitmq.ack.customer
 * 版本信息: 版本1.0
 * 日期:2019/11/12
 * Copyright XXXXXX科技有限公司
 */


/**
 * @describe：这个使用通配符，需要自定义处理
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/12 18:48
 */
public class TopicRichReceiver {
}
