package com.lvmoney.rabbitmq.ack.factory;/**
 * 描述:
 * 包名:com.lvmoney.rabbitmq.ack.factory
 * 版本信息: 版本1.0
 * 日期:2019/11/12
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.common.exception.BusinessException;
import com.lvmoney.common.exception.CommonException;
import com.lvmoney.rabbitmq.ack.annotation.DyProviderService;
import com.lvmoney.rabbitmq.ack.constant.RabbitmqConstant;
import com.lvmoney.rabbitmq.ack.provider.MqSender;
import com.lvmoney.rabbitmq.ack.service.HandMqDataService;
import com.lvmoney.rabbitmq.ack.util.ContextBeanUtil;
import com.lvmoney.rabbitmq.ack.vo.MessageVo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.lvmoney.rabbitmq.ack.constant.RabbitmqConstant.MQ_TYPE_TOPIC_RICH;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/12 18:17
 */
@Component
public class HandMqSenderContext {
    private static final Logger LOGGER = LoggerFactory.getLogger(HandMqSenderContext.class);

    @Autowired
    private Map<String, MqSender> strategyMap = new ConcurrentHashMap<>();

    /**
     * @describe:策略注入
     * @author: lvmoney /xxxx科技有限公司
     * 2018年11月8日下午3:08:36
     */
    @Autowired
    public <T> HandMqSenderContext(Map<String, MqSender> strategyMap) {
        this.strategyMap.clear();
        strategyMap.forEach((k, v) -> this.strategyMap.put(k, v));

    }

    public void sendMsg(String implName, MessageVo messageVo) {


        if (StringUtils.isEmpty(messageVo.getMsgType())) {
            throw new BusinessException(CommonException.Proxy.RABBIT_MSG_TYPE_NOT_NULL);
        }
        if (RabbitmqConstant.MQ_TYPE_RANOUT.equals(implName)
                || RabbitmqConstant.MQ_TYPE_SIMPLE.equals(implName) ||
                RabbitmqConstant.MQ_TYPE_TOPIC.equals(implName) ||
                MQ_TYPE_TOPIC_RICH.equals(implName)
        ) {
            String beanName = getMqSenderBeanName(implName);
            if (StringUtils.isEmpty(beanName)) {
                throw new BusinessException(CommonException.Proxy.RABBITMQ_PROVIDER_DYNAMIC_NOT_EXSIT);

            }
            strategyMap.get(beanName).send(messageVo);
        } else {
            LOGGER.warn("rabbitmq发送消息找不到对应的发送器");
            throw new BusinessException(CommonException.Proxy.RABBIT_SENDER_TYPE_NOT_SUPPORT);
        }

    }

    public Map<String, MqSender> getStrategyMap() {
        return strategyMap;
    }

    private String getMqSenderBeanName(String mqType) {
        Map<String, MqSender> map = getStrategyMap();
        for (Map.Entry<String, MqSender> entry : map.entrySet()) {
            Class<? extends MqSender> clazz = entry.getValue().getClass();
            if (clazz.isAnnotationPresent(DyProviderService.class)) {
                DyProviderService dynamicService = clazz.getAnnotation(DyProviderService.class);
                String name = dynamicService.name();
                if (name.equals(mqType)) {
                    return entry.getKey();
                }
            }
        }
        return "";
    }
}
