/**
 * 描述:
 * 包名:com.lvmoney.pay.factory
 * 版本信息: 版本1.0
 * 日期:2018年10月18日  上午11:07:01
 * Copyright xxxx科技有限公司
 */

package com.lvmoney.rabbitmq.ack.factory;

import com.lvmoney.common.exception.BusinessException;
import com.lvmoney.common.exception.CommonException;
import com.lvmoney.common.util.StringUtil;
import com.lvmoney.rabbitmq.ack.annotation.DyCustomerService;
import com.lvmoney.rabbitmq.ack.service.HandMqDataService;
import com.lvmoney.rabbitmq.ack.vo.MessageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @describe：调用不同数据处理策略模式环境角色类
 * @author: lvmoney /xxxx科技有限公司
 * @version:v1.0 2018年10月18日 上午11:07:01
 */
@Component
public class HandMqServiceContext {
    @Autowired
    private Map<String, HandMqDataService> strategyMap = new ConcurrentHashMap<>();

    /**
     * @describe:策略注入
     * @author: lvmoney /xxxx科技有限公司
     * 2018年11月8日下午3:08:36
     */
    @Autowired
    public <T> HandMqServiceContext(Map<String, HandMqDataService> strategyMap) {
        this.strategyMap.clear();
        strategyMap.forEach((k, v) -> this.strategyMap.put(k, v));

    }

    /**
     * @param implName
     * @param messageVo 2018年11月8日下午3:08:26
     * @describe:策略方法
     * @author: lvmoney /xxxx科技有限公司
     */
    @SuppressWarnings("rawtypes")
    public void getData(String implName, MessageVo messageVo) {
        String beanName = getHandMqDataServiceBeanName(implName);
        if (StringUtil.isBlank(beanName)) {
            throw new BusinessException(CommonException.Proxy.RABBITMQ_DYNAMIC_NOT_EXSIT);
        }
        strategyMap.get(beanName).handData(messageVo);
    }

    public Map<String, HandMqDataService> getStrategyMap() {
        return strategyMap;
    }

    private String getHandMqDataServiceBeanName(String mqType) {
        Map<String, HandMqDataService> map = this.getStrategyMap();
        for (Map.Entry<String, HandMqDataService> entry : map.entrySet()) {
            Class<? extends HandMqDataService> clazz = entry.getValue().getClass();
            if (clazz.isAnnotationPresent(DyCustomerService.class)) {
                DyCustomerService dynamicService = clazz.getAnnotation(DyCustomerService.class);
                String name = dynamicService.name();
                if (name.equals(mqType)) {
                    return entry.getKey();
                }
            }
        }
        return "";
    }

}
