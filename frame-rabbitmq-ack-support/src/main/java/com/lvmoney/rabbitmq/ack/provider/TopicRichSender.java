package com.lvmoney.rabbitmq.ack.provider;/**
 * 描述:
 * 包名:com.lvmoney.rabbitmq.ack.provider
 * 版本信息: 版本1.0
 * 日期:2019/11/12
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.common.util.JsonUtil;
import com.lvmoney.rabbitmq.ack.annotation.DyProviderService;
import com.lvmoney.rabbitmq.ack.constant.RabbitmqConstant;
import com.lvmoney.rabbitmq.ack.ro.AckErrorRecordRo;
import com.lvmoney.rabbitmq.ack.service.RabbitmqRedisService;
import com.lvmoney.rabbitmq.ack.vo.MessageVo;
import com.lvmoney.rabbitmq.ack.vo.MsgErrorVo;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.SerializerMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.lvmoney.rabbitmq.ack.constant.RabbitmqConstant.MQ_TYPE_TOPIC_RICH;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/12 18:44
 */
@Component
@DyProviderService(name = MQ_TYPE_TOPIC_RICH)
public class TopicRichSender implements MqSender {
    @Autowired
    RabbitmqRedisService rabbitmqRedisService;
    @Value("${rabbitmq.error.record.expire:18000}")
    String expire;
    @Autowired
    ConnectionFactory connectionFactory;

    @Bean("topicRichRabbitTemplate")
    @Scope("prototype")
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMandatory(true);
        template.setMessageConverter(new SerializerMessageConverter());
        return template;
    }

    /**
     * @describe: 通配符匹配一topic开头的队列
     * @param: [msg]
     * @return: void
     * @author： lvmoney /xxxx科技有限公司
     * 2019/1/21
     */
    @Override
    public void send(MessageVo msg) {
        RabbitTemplate rabbitTemplate = rabbitTemplate(connectionFactory);
        rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            //只要exchange 错误被调用，ack=false
            if (!ack) {
                AckErrorRecordRo ackErrorRecordRo = new AckErrorRecordRo();
                ackErrorRecordRo.setExpire(Long.valueOf(expire));
                MsgErrorVo msgErrorVo = new MsgErrorVo();
                msgErrorVo.setMessageVo(msg);
                msgErrorVo.setMqName(RabbitmqConstant.SIMPLE_QUEUE_NAME);
                List<MsgErrorVo> data = new ArrayList<>();
                data.add(msgErrorVo);
                ackErrorRecordRo.setMsgErrorVoList(data);
                rabbitmqRedisService.ackRecord2Redis(ackErrorRecordRo);
            }
        });
        rabbitTemplate.setReturnCallback((Message message, int replyCode, String replyText,
                                          String exchange, String routingKey) -> { //exchange 正确,queue 错误 ,confirm被回调, ack=true; return被回调 replyText:NO_ROUTE
            AckErrorRecordRo ackErrorRecordRo = new AckErrorRecordRo();
            ackErrorRecordRo.setExpire(Long.valueOf(expire));
            MsgErrorVo msgErrorVo = new MsgErrorVo();
            msgErrorVo.setMessageVo(msg);
            msgErrorVo.setMqName(RabbitmqConstant.SIMPLE_QUEUE_NAME);
            List<MsgErrorVo> data = new ArrayList<>();
            data.add(msgErrorVo);
            ackErrorRecordRo.setMsgErrorVoList(data);
            rabbitmqRedisService.ackRecord2Redis(ackErrorRecordRo);
        });
        rabbitTemplate.convertAndSend(RabbitmqConstant.EXCHANGE_TOPIC, RabbitmqConstant.MESSAGE_TOPICS, JsonUtil.t2JsonString(msg));
    }
}
