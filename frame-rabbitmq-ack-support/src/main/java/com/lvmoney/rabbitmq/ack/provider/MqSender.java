package com.lvmoney.rabbitmq.ack.provider;/**
 * 描述:
 * 包名:com.lvmoney.rabbitmq.ack.provider
 * 版本信息: 版本1.0
 * 日期:2019/11/12
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.rabbitmq.ack.vo.MessageVo;

/**
 * @describe：统一的发送接口
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/12 18:10
 */
public interface MqSender {

    /**
     * 发送消息
     *
     * @param msg: 消息体
     * @throws
     * @return: void
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/11/12 18:12
     */
    void send(MessageVo msg);
}
