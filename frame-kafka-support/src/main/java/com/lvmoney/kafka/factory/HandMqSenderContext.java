package com.lvmoney.kafka.factory;/**
 * 描述:
 * 包名:com.lvmoney.kafka.factory
 * 版本信息: 版本1.0
 * 日期:2019/11/13
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.common.exception.BusinessException;
import com.lvmoney.common.exception.CommonException;
import com.lvmoney.kafka.annotation.DyProviderService;
import com.lvmoney.kafka.constant.KafkaConstant;
import com.lvmoney.kafka.provider.MqSender;
import com.lvmoney.kafka.vo.MessageVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/13 9:49
 */
@Component
public class HandMqSenderContext {
    @Autowired
    private Map<String, MqSender> strategyMap = new ConcurrentHashMap<>();

    /**
     * @describe:策略注入
     * @author: lvmoney /xxxx科技有限公司
     * 2018年11月8日下午3:08:36
     */
    @Autowired
    public <T> HandMqSenderContext(Map<String, MqSender> strategyMap) {
        this.strategyMap.clear();
        strategyMap.forEach((k, v) -> this.strategyMap.put(k, v));

    }

    /**
     * @param implName
     * @param messageVo 2018年11月8日下午3:08:26
     * @describe:策略方法
     * @author: lvmoney /xxxx科技有限公司
     */
    @SuppressWarnings("rawtypes")
    public void sendMsg(String implName, MessageVo messageVo) {

        if (StringUtils.isEmpty(messageVo.getMsgType())) {
            throw new BusinessException(CommonException.Proxy.KAFKA_MSG_TYPE_NOT_NULL);
        }
        if (KafkaConstant.KAFAK_TYPE_SIMPLE.equals(implName)
                || KafkaConstant.KAFKA_TYPE_SYN.equals(implName)) {
            String beanName = getBeanName(implName);
            if (StringUtils.isEmpty(beanName)) {
                throw new BusinessException(CommonException.Proxy.KAFKA_PROVIDER_DYNAMIC_NOT_EXSIT);
            }
            strategyMap.get(beanName).send(messageVo);
        } else {
            throw new BusinessException(CommonException.Proxy.KAFKA_SENDER_TYPE_NOT_SUPPORT);
        }


        String beanName = getBeanName(implName);
        if (StringUtils.isBlank(beanName)) {
            throw new BusinessException(CommonException.Proxy.KAFKA_PROVIDER_DYNAMIC_NOT_EXSIT);
        }
        strategyMap.get(beanName).send(messageVo);
    }

    public Map<String, MqSender> getStrategyMap() {
        return strategyMap;
    }

    private String getBeanName(String mqType) {
        Map<String, MqSender> map = this.getStrategyMap();
        for (Map.Entry<String, MqSender> entry : map.entrySet()) {
            Class<? extends MqSender> clazz = entry.getValue().getClass();
            if (clazz.isAnnotationPresent(DyProviderService.class)) {
                DyProviderService dynamicService = clazz.getAnnotation(DyProviderService.class);
                String name = dynamicService.name();
                if (name.equals(mqType)) {
                    return entry.getKey();
                }
            }
        }
        return "";
    }

}
