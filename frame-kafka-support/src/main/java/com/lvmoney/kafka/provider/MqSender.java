package com.lvmoney.kafka.provider;/**
 * 描述:
 * 包名:com.lvmoney.kafka.provider
 * 版本信息: 版本1.0
 * 日期:2019/11/13
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.kafka.vo.MessageVo;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/13 9:47
 */
public interface MqSender {
    /**
     * 发送消息
     *
     * @param messageVo:
     * @throws
     * @return: void
     * @author: lvmoney /XXXXXX科技有限公司
     * @date: 2019/11/13 9:48
     */
    void send(MessageVo messageVo);
}
