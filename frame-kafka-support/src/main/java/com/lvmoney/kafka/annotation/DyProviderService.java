package com.lvmoney.kafka.annotation;/**
 * 描述:
 * 包名:com.lvmoney.kafka.annotation
 * 版本信息: 版本1.0
 * 日期:2019/11/13
 * Copyright XXXXXX科技有限公司
 */


import java.lang.annotation.*;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/13 9:51
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface DyProviderService {
    String name() default "";
}
