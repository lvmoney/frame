# frame

#### 介绍

分布式后台常用技术完全整合架构。通过maven依赖简单的引入即可使用各种常用技术。天然支持k8s+istio的云架构。

#### 使用方式

通过maven依赖的方式引用到项目模块即可。把frame作为所有项目的parent，子项目一般都要引入common，其他模块根据系统需要引入，详见各个module的readme.md。架构使用的各个中间件的centos7版集群安装操作和使用手册均已提交到百度云盘

#### 服务支持

微服务基础框架  
下一代微服务istio支持  
docker支持  
消息队列  
分布式事务  
文件存储  
全文检索  
oauth2  
工作流引擎  
权限  
分布式锁  
读写分离   
规则引擎  
验证码  
netty  
定时任务  
大数据架构  
等等


#### 技术罗列

springboot  
springcloud  
docker  
k8s1.16.0  
istio1.3.0   
flink   
hadoop  
shiro  
oauth2  
activti  
jwt  
kafka  
rabbitmq  
seata(fescar)  
mongo  
mybatisplus  
等等


#### 可行性

大部分技术已用到正式项目环境，以校验整体技术框架的正确性和可行性

#### 代码风格

开发的时候用了阿里代码的扫描工具，除某些测试代码均需通过扫描工具的验证

#### 支持力度

后台基础框架版v1.0已经完成 

整个技术实现正在不断的完善

#### 个人推荐

 :point_right:  **_推荐使用istio去解决问题，详情可以查看frame-k8s-support来实现_**  :point_left: 


#### 参与贡献

框架构架和开发人员:  

花名：lvmoney  
qq:1300515928  


#### 其他进度

微服务架构版:https://gitee.com/lvmoney/lvmoney-frame-parent  



