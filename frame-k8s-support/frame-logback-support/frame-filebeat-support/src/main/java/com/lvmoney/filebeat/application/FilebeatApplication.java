package com.lvmoney.filebeat.application;/**
 * 描述:
 * 包名:com.lvmoney.filebeat.application
 * 版本信息: 版本1.0
 * 日期:2019/11/14
 * Copyright XXXXXX科技有限公司
 */


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/14 16:45
 */
@SpringBootApplication(scanBasePackages = {"com.lvmoney.**"})
public class FilebeatApplication {
    public static void main(String[] args) {
        SpringApplication.run(FilebeatApplication.class, args);
    }

}
