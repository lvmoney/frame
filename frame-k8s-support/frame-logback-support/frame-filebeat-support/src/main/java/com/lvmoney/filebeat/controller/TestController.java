package com.lvmoney.filebeat.controller;/**
 * 描述:
 * 包名:com.lvmoney.filebeat.controller
 * 版本信息: 版本1.0
 * 日期:2019/11/14
 * Copyright XXXXXX科技有限公司
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/14 16:46
 */
@RestController
public class TestController {
    private final static Logger log = LoggerFactory.getLogger(TestController.class);
    @Value("${LOGSTASH_HOST}")
    private String ip;

    @GetMapping("fTest")
    public void test() {
        log.info("ip:{}的filebeat  测试 info 成功了！！！", ip);
        log.warn("ip:{}的filebeat   测试 warn 成功了！！！", ip);
        log.error("ip:{}的filebeat   测试 error 成功了！！", ip);
    }
}
