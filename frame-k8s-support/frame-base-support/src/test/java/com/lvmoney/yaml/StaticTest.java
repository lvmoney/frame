package com.lvmoney.yaml;/**
 * 描述:
 * 包名:com.lvmoney.yaml
 * 版本信息: 版本1.0
 * 日期:2020/5/25
 * Copyright XXXXXX科技有限公司
 */


import java.util.function.Consumer;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2020/5/25 9:30
 */
public class StaticTest {
    public static void write(String str) {
        System.out.println(str);
    }

    public static void write2() {
        System.out.println("asdfadsf");
    }

    public static void write3(String test) {
        System.out.println("test");
    }

    public static void write4() {
        System.out.println("test4");
    }


    public static void main(String[] args) {
        Consumer<String> methodParam = StaticTest::write;
        methodParam.accept("121212");
        Consumer<String> consumer = StaticTest::write3;
        consumer.accept("test");
        Runnable write2 = StaticTest::write2;
        write2.run();
        Runnable write5 = StaticTest::write4;
    }
}
