package com.lvmoney.mq.common.ro;/**
 * 描述:
 * 包名:com.lvmoney.mq.common.ro
 * 版本信息: 版本1.0
 * 日期:2019/11/21
 * Copyright XXXXXX科技有限公司
 */


import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/21 9:21
 */
@Data
public class ErrorRecordRo<T> implements Serializable {
    private static final long serialVersionUID = -4043990236650268838L;
    private List<T> data;
    private String type;
    private Long expire;
}
