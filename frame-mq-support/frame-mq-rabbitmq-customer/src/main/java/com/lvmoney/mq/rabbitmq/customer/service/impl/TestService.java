package com.lvmoney.mq.rabbitmq.customer.service.impl;/**
 * 描述:
 * 包名:com.lvmoney.mq.rabbitmq.customer.service.impl
 * 版本信息: 版本1.0
 * 日期:2019/11/20
 * Copyright XXXXXX科技有限公司
 */


import com.lvmoney.common.util.JsonUtil;
import com.lvmoney.mq.common.annation.CustomerService;
import com.lvmoney.mq.common.service.MqDataHandService;
import com.lvmoney.mq.common.vo.MessageVo;

/**
 * @describe：
 * @author: lvmoney/XXXXXX科技有限公司
 * @version:v1.0 2019/11/20 18:17
 */
@CustomerService(name = "simple")
public class TestService implements MqDataHandService {
    @Override
    public void handData(MessageVo messageVo) {
        System.out.println(JsonUtil.t2JsonString(messageVo));
    }
}
