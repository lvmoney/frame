package com.lvmoney.mq.rabbitmq.provider.controller;


import com.lvmoney.mq.common.annation.MqImpl;
import com.lvmoney.mq.common.constant.MqConstant;
import com.lvmoney.mq.common.service.MqSendService;
import com.lvmoney.mq.common.vo.MessageVo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @describe：
 * @author: lvmoney /xxxx科技有限公司
 * @version:v1.0 2018年9月30日 上午8:51:33
 */
@RestController
@RequestMapping("/send")
public class BaseController {
    @MqImpl(MqConstant.KAFAK_TYPE_SIMPLE)
    private MqSendService mqSendService;

    @RequestMapping(value = "one2one", method = RequestMethod.GET)
    public void one2one() {
        MessageVo messageVo = new MessageVo();
        Date date = new Date();
        long dateTime = date.getTime();
        messageVo.setDate(dateTime);
        messageVo.setMsgType("simple");
        mqSendService.send(messageVo);
    }
}

