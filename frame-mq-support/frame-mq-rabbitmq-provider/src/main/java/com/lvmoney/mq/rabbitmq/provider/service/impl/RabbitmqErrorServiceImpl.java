package com.lvmoney.mq.rabbitmq.provider.service.impl;/**
 * 描述:
 * 包名:com.lvmoney.jwt.annotation
 * 版本信息: 版本1.0
 * 日期:2019/1/21
 * Copyright xxxx科技有限公司
 */


import com.lvmoney.mq.rabbitmq.provider.service.RabbitmqErrorService;
import com.lvmoney.cache.common.annation.CacheImpl;
import com.lvmoney.cache.common.constant.CacheConstant;
import com.lvmoney.cache.redis.service.BaseRedisService;
import com.lvmoney.mq.common.annation.MqService;
import com.lvmoney.mq.common.constant.MqConstant;
import com.lvmoney.mq.common.ro.ErrorRecordRo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.lvmoney.mq.common.constant.MqConstant.MQ_ERROR_TYPE_RABBIT;

/**
 * @describe：
 * @author: lvmoney /xxxx科技有限公司
 * @version:v1.0 2018年10月30日 下午3:29:38
 */
@MqService(MQ_ERROR_TYPE_RABBIT)
public class RabbitmqErrorServiceImpl extends RabbitmqErrorService {
    @Autowired
    BaseRedisService baseRedisService;


    @Override
    public void errorRecord2Redis(ErrorRecordRo errorRecordRo) {
        baseRedisService.addList(MqConstant.MQ_ERRROR_RECORD_REDIS_KEY, errorRecordRo.getData(), errorRecordRo.getExpire());
    }

    @Override
    public List getAllErrorRecord() {
        return baseRedisService.getListAll(MqConstant.MQ_ERRROR_RECORD_REDIS_KEY);
    }
}
